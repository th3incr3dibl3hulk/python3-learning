import random

players = ["Martin", "Craig", "Sue", "Claire", "Dave", "Alice", "Luciana", "Harry", "Jack",
"Rose", "Lexi", "Maria", "Thomas", "James", "William", "Ada", "Grace", "Jean", "Marissa", "Alan"]

print("Welcome to Team/Player Allocator!")


while True:
    random.shuffle(players)
    response = input("Is it a team or individual sport? \n Type team or individual: ")
    if response == "team":

    #picking team 1 and captain
        team1 = players[:len(players)//2]
        print("Team 1 captain: "+random.choice(team1))
        print("Team 1: ")
        for player in team1:
            print(player)

    #picking team 2 and captain
        team2 = players[len(players)//2:]
        print("\nTeam 2 captain: "+random.choice(team2))
        print("Team 2: ")
        for player in team2:
            print(player)

    #Break to stop cycling the program
        response = input("Pick teams again? Type y or n: ")
        if response == "n":
            break
    else:
        for i in range(0,20,2):
            print(players[i] + " vs " + players[i+1])
            start = random.randrange(i, i+2)
            print(players[start] + " starts")
